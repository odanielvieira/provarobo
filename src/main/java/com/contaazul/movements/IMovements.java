package com.contaazul.movements;

import com.contaazul.customException.RobotCollisionException;

public interface IMovements {
	
	abstract void moveForward() throws RobotCollisionException;
	abstract void moveBackward() throws RobotCollisionException;
	abstract void moveLeft() throws RobotCollisionException;
	abstract void moveRight() throws RobotCollisionException;
	
	abstract void eastRotation();
	abstract void westRotation();
	abstract void southRotation();
	abstract void northRotation();
	
}
