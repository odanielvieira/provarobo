package com.contaazul.ws;


import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.contaazul.customException.InvalidCommandException;
import com.contaazul.customException.RobotCollisionException;
import com.contaazul.model.Robot;
import com.contaazul.service.RobotService;

@Path("/mars")
public class RobotMovesWS {

	private Robot robot;
	private RobotService robotService;
	
	@GET
	@Path("/{directions}")
	@Produces("text/plain")
	public Response moveRoboGet(@PathParam("directions") String directions) {
		robot = new Robot();
		robotService = new RobotService(robot);
		try {
			robotService.moveRobot(directions);
			return Response.status(Status.OK).entity(robot.getFormattedPosition()).type(MediaType.TEXT_PLAIN).build();
		} catch (InvalidCommandException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage() ).type(MediaType.TEXT_PLAIN).build();
		} catch (RobotCollisionException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage() ).type(MediaType.TEXT_PLAIN).build();
		}
	}
	
	@POST
	@Path("/{directions}")
	@Produces("text/plain")
	public Response moveRoboPost(@PathParam("directions") String directions) {
		robot = new Robot();
		robotService = new RobotService(robot);
		try {
			robotService.moveRobot(directions);
			return Response.status(Status.OK).entity(robot.getFormattedPosition()).type(MediaType.TEXT_PLAIN).build();
		} catch (InvalidCommandException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage() ).type(MediaType.TEXT_PLAIN).build();
		} catch (RobotCollisionException e) {
			e.printStackTrace();
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(e.getMessage() ).type(MediaType.TEXT_PLAIN).build();
		}
	}	
	
}
