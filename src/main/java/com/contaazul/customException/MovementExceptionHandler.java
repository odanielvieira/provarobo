package com.contaazul.customException;


import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


@Provider
public class MovementExceptionHandler implements ExceptionMapper<NotFoundException> {
	private final String erroMessage = "Sorry! We couldn't understand your request. Please check url, path and parameters to complete your request.";

	@Override
	public Response toResponse(NotFoundException arg0) {
        return Response.status(Status.NOT_FOUND)
                .entity(erroMessage).type(MediaType.TEXT_PLAIN)
                .build();
	}
}