package com.contaazul.customException;

public class RobotCollisionException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7592644272910759966L;

	public RobotCollisionException() {
		super();
	}

	public RobotCollisionException(String message) {
		super(message); 
	}
	
	public RobotCollisionException(String message, Throwable cause) { 
		super(message, cause); 
	}
	
	public RobotCollisionException(Throwable cause) { 
		super(cause); 
	}
}
