package com.contaazul.customException;

public class InvalidCommandException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5947227942724624936L;

	public InvalidCommandException() {
		super();
	}

	public InvalidCommandException(String message) {
		super(message); 
	}
	
	public InvalidCommandException(String message, Throwable cause) { 
		super(message, cause); 
	}
	
	public InvalidCommandException(Throwable cause) { 
		super(cause); 
	}
	
}
