package com.contaazul.enums;

public enum MovementInstructionEnum {
	LEFT('L'), RIGHT('R'), MOVE('M');
	
	private char movementInstruction;
	
	private MovementInstructionEnum(char movementInstruction) {
		this.movementInstruction = movementInstruction;
	}
	
	public char getValue() {
		return movementInstruction;
	}
	
}
