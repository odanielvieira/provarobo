package com.contaazul.enums;

public enum FaceEnum {
	
	NORTH('N'), SOUTH('S'), EAST('E'), WEST('W');
	
	private char face;
	
	private FaceEnum(char face) {
		this.face = face;
	}
	
	public char getValue() {
		return face;
	}

}
