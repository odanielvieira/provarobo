package com.contaazul.util;

import com.contaazul.enums.MovementInstructionEnum;

public class CommandParser {
	
	/**
	 * parse an input String into a char array
	 * @param movementStructions
	 * @return
	 */
	public char[] rawMovementsToCharArray(String movementStructions) {
		return movementStructions.toCharArray();
	}
	
	/**
	 * check if the input instruction is a rotation movement
	 * @param movement
	 * @return
	 */
	public boolean isRotationMovement(char movement) {
 	  return (Character.toUpperCase(movement) == MovementInstructionEnum.LEFT.getValue() 
 			  || Character.toUpperCase(movement) == MovementInstructionEnum.RIGHT.getValue());
	}
	
	/**
	 * check if an input instruction is a movement command
	 * @param movement
	 * @return
	 */
	public boolean isMovementInstruction(char movement) {
	 	  return (Character.toUpperCase(movement) == MovementInstructionEnum.MOVE.getValue()); 
	}
	
	/**
	 * check if the robot is trying to turn right
	 * @param direction
	 * @return true if robot is trying to move right
	 */
	public boolean isRightRotation(char direction) {
		return Character.toUpperCase(direction) == MovementInstructionEnum.RIGHT.getValue();
	}
	
	/**
	 * check if robot is trying to turn left
	 * @param direction
	 * @return true if robot is trying to move left
	 */
	public boolean isLeftRotation(char direction) {
		return Character.toUpperCase(direction) == MovementInstructionEnum.LEFT.getValue();
	}
}
