package com.contaazul.model;

public class LandArea {

	private final int maxPosition;
	private final int minPosition;		

	public LandArea(int maxPosition, int minPosition) {
		this.maxPosition = 5;
		this.minPosition = 0;
	}
	
	public int getMaxPosition() {
		return maxPosition;
	}

	public int getMinPosition() {
		return minPosition;
	}
}
