package com.contaazul.model;

import java.text.MessageFormat;

import com.contaazul.customException.InvalidCommandException;
import com.contaazul.customException.RobotCollisionException;
import com.contaazul.enums.FaceEnum;
import com.contaazul.movements.IMovements;
import com.contaazul.service.RobotService;

public class Robot implements IMovements {

	private int xNodePosition;
	private int yNodePosition;
	private char face;
	private String robotPositionMessage = "({0}, {1}, {2})";
	private RobotService robotService;
	
	public Robot() {
		this.xNodePosition = 0;
		this.yNodePosition = 0;
		this.face = FaceEnum.NORTH.getValue();
		this.robotService = new RobotService(this);
	}
	
	/**
	 * rotate robot to west
	 */
	@Override
	public void westRotation() {
		this.face = FaceEnum.WEST.getValue();		
	}
	
	/**
	 * rotate robot to east
	 */
	@Override
	public void eastRotation() {
		this.face = FaceEnum.EAST.getValue();
	}
	
	/**
	 * rotate robot to north
	 */
	@Override
	public void northRotation() {
		this.face = FaceEnum.NORTH.getValue();
	}
	
	/**
	 * rotate robot to south
	 */
	@Override
	public void southRotation() {
		this.face = FaceEnum.SOUTH.getValue();
	}	


	/**
	 * move robot one step forward
	 * @throws RobotCollisionException when robot reaches at any map boundary
	 */
	@Override
	public void moveForward() throws RobotCollisionException {
		int futurePosition = this.yNodePosition + 1;
		robotService.checkCollision(futurePosition);
		this.yNodePosition = futurePosition;
	}
	
	/**
	 * move robot one step backward
	 * @throws RobotCollisionException when robot reaches at any map boundary
	 */
	@Override 
	public void moveBackward() throws RobotCollisionException {
		int futurePosition = this.yNodePosition - 1;
		robotService.checkCollision(futurePosition);
		this.yNodePosition = futurePosition;		
	}
	
	/**
	 * move robot one step left
	 * @throws RobotCollisionException when robot reaches at any map boundary
	 */
	@Override 
	public void moveLeft() throws RobotCollisionException {
		int futurePosition = this.xNodePosition - 1;
		robotService.checkCollision(futurePosition);
		this.xNodePosition = futurePosition;		
	}
	
	/**
	 * move robot one step right
	 * @throws RobotCollisionException when robot reaches at any map boundary
	 */
	@Override 
	public void moveRight() throws RobotCollisionException {
		int futurePosition = this.xNodePosition + 1;
		robotService.checkCollision(futurePosition);
		this.xNodePosition = futurePosition;
	}
	
	/**
	 * rotate right, from current face position
	 * @throws InvalidCommandException 
	 */
	public void rotateRobotRight() {
		
		switch (Character.toUpperCase(face)) {
			case 'N' : eastRotation();
				break;
				
			case 'S' : westRotation();
				break;			
	
			case 'E' : southRotation();
				break;
	
			case 'W' : northRotation();
				break;			
		}
	}
	
	/**
	 * rotate left, from current face position
	 * @throws InvalidCommandException 
	 */
	public void rotateRobotLeft() {
		
		switch (Character.toUpperCase(getFace())) {
			case 'N' : westRotation();
				break;
				
			case 'S' : eastRotation();
				break;			
	
			case 'E' : northRotation();
				break;
	
			case 'W' : southRotation();
				break;			

		}
		
	}	
	
	/**
	 * move robot into map, according to current face
	 * @throws RobotCollisionException when robot reaches at any map boundary
	 */
	public void moveRobot() throws RobotCollisionException {
		
		switch (Character.toUpperCase(getFace())) {
			case 'N' : moveForward();
				break;
				
			case 'S' : moveBackward();
				break;			
	
			case 'W' : moveLeft();
				break;
	
			case 'E' : moveRight();
				break;			
		}		
	}
	
	/**
	 * get curret robot position formatted in (X_POSITION, Y_POSITION, FACE)
	 * @return formatted String with current robot position  
	 */
	public String getFormattedPosition() {
		return MessageFormat.format(robotPositionMessage, getXNodePosition(), getYNodePosition(), getFace());
	}	
	
	public int getXNodePosition() {
		return this.xNodePosition;
	}

	public int getYNodePosition() {
		return this.yNodePosition;
	}

	public char getFace() {
		return face;
	}
}
