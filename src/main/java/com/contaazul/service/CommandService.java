//package com.contaazul.service;
//
//import com.contaazul.customException.InvalidCommandException;
//import com.contaazul.customException.RobotCollisionException;
//import com.contaazul.enums.MovementInstructionEnum;
//import com.contaazul.model.Robot;
//
//public class CommandService {
//	
//	private Robot robot;
//	
//	public CommandService(Robot robot) {
//		this.robot = robot; 
//	}
//	
//	/**
//	 * move the robot using incoming commands from user
//	 * @param commandInstructions
//	 * @throws InvalidCommandException
//	 * @throws RobotCollisionException
//	 */
//	public void moveRobot(String commandInstructions) throws InvalidCommandException, RobotCollisionException {
//		char[] commands = rawMovementsToCharArray(commandInstructions);
//		
//		for(char command : commands) {
//			if(isRotationMovement(command)) {
//				rotationProxy(command);
//			} else if(isMovementInstruction(command)) {
//				moveRobot();
//			} else {
//				throw new InvalidCommandException("Command: " + command + " is not a valid command");
//			}
//		}
//	}	
//	
//	/**
//	 * check instruction command type.
//	 * @param command
//	 * @throws InvalidCommandException
//	 */
//	private void rotationProxy(char command) throws InvalidCommandException {
//		if(isRightRotation(command)) {
//			rotateRobotRight();
//		} else if(isLeftRotation(command)) {
//			rotateRobotLeft();
//		}
//	}
//	
//	
//	/**
//	 * parse an input String into a char array
//	 * @param movementStructions
//	 * @return
//	 */
//	private char[] rawMovementsToCharArray(String movementStructions) {
//		return movementStructions.toCharArray();
//	}
//	
//	/**
//	 * check if the input instruction is a rotation movement
//	 * @param movement
//	 * @return
//	 */
//	private boolean isRotationMovement(char movement) {
// 	  return (Character.toUpperCase(movement) == MovementInstructionEnum.LEFT.getValue() 
// 			  || Character.toUpperCase(movement) == MovementInstructionEnum.RIGHT.getValue());
//	}
//	
//	/**
//	 * check if an input instruction is a movement command
//	 * @param movement
//	 * @return
//	 */
//	private boolean isMovementInstruction(char movement) {
//	 	  return (Character.toUpperCase(movement) == MovementInstructionEnum.MOVE.getValue()); 
//	}
//	
//	/**
//	 * check if the robot is trying to turn right
//	 * @param direction
//	 * @return true if robot is trying to move right
//	 */
//	private boolean isRightRotation(char direction) {
//		return Character.toUpperCase(direction) == MovementInstructionEnum.RIGHT.getValue();
//	}
//	
//	/**
//	 * check if robot is trying to turn left
//	 * @param direction
//	 * @return true if robot is trying to move left
//	 */
//	private boolean isLeftRotation(char direction) {
//		return Character.toUpperCase(direction) == MovementInstructionEnum.LEFT.getValue();
//	}
//
//}
