package com.contaazul.service;

import com.contaazul.customException.InvalidCommandException;
import com.contaazul.customException.RobotCollisionException;
import com.contaazul.model.LandArea;
import com.contaazul.model.Robot;
import com.contaazul.util.CommandParser;

public class RobotService {
	
	private Robot robot;
	private LandArea landArea;
	private CommandParser commandParser;
	
	public RobotService(Robot robot) {
		this.robot = robot;
		this.landArea = new LandArea(5, 5);
		this.commandParser = new CommandParser();
	}

	/**
	 * move the robot using incoming commands from user
	 * @param commandInstructions
	 * @throws InvalidCommandException
	 * @throws RobotCollisionException
	 */
	public void moveRobot(String commandInstructions) throws InvalidCommandException, RobotCollisionException {
		char[] commands = commandParser.rawMovementsToCharArray(commandInstructions);
		
		for(char command : commands) {
			if(commandParser.isRotationMovement(command)) {
				rotationProxy(command);
			} else if(commandParser.isMovementInstruction(command)) {
				robot.moveRobot();
			} else {
				throw new InvalidCommandException("Command: " + command + " is not a valid command");
			}
		}
	}
	
	/**
	 * check instruction command type.
	 * @param command
	 * @throws InvalidCommandException
	 */
	private void rotationProxy(char command) throws InvalidCommandException {
		if(commandParser.isRightRotation(command)) {
			robot.rotateRobotRight();
		} else if(commandParser.isLeftRotation(command)) {
			robot.rotateRobotLeft();
		}
	}		
	
	/**
	 * Method used to verify if robot hit a point of impact
	 * @param nextPosition
	 * @return true if robot has reached his limit boundaries
	 * @throws RobotCollisionException 
	 */
	public void checkCollision(int nextPosition) throws RobotCollisionException {
		if(didCollided(nextPosition)) {
			throw new RobotCollisionException("Robot collided at " + robot.getFormattedPosition() );
		}
	}

	/**
	 *
	 * @param nextPosition
	 * @return true if next position is a point of collision
	 */
	private boolean didCollided(int nextPosition) {
		return nextPosition > landArea.getMaxPosition() || nextPosition < landArea.getMinPosition();
	}
	
}
