package rotation;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.contaazul.customException.InvalidCommandException;
import com.contaazul.customException.RobotCollisionException;
import com.contaazul.enums.FaceEnum;
import com.contaazul.model.Robot;
import com.contaazul.service.RobotService;


public class RotationTest {

	private Robot robot;
	private RobotService robotService;
	
	@Before
	public void resetRobot() {
		robot = new Robot();
		robotService = new RobotService(robot);
		
	}
	
	@Test
	public void rotateLeft() throws InvalidCommandException {
		robotService.moveRobot("l");
		assertEquals(FaceEnum.WEST.getValue(), robot.getFace());
	}
	
	@Test
	public void rotateRight() throws InvalidCommandException {
		robotService.moveRobot("r");
		assertEquals(FaceEnum.EAST.getValue(), robot.getFace());
	}	
	
	@Test
	public void rotate180eRight() throws InvalidCommandException {
		robotService.moveRobot("rr");
		assertEquals(FaceEnum.SOUTH.getValue(), robot.getFace());
	}
	
	@Test
	public void rotate270Right() throws InvalidCommandException {
		robotService.moveRobot("rrr");
		assertEquals(FaceEnum.WEST.getValue(), robot.getFace());
	}
	
	@Test
	public void rotate360Right() throws InvalidCommandException {
		robotService.moveRobot("rrrr");
		assertEquals(FaceEnum.NORTH.getValue(), robot.getFace());
	}		
	
	@Test
	public void rotate180Left() throws InvalidCommandException {
		robotService.moveRobot("ll");
		assertEquals(FaceEnum.SOUTH.getValue(), robot.getFace());
	}
	
	@Test
	public void rotate270Left() throws InvalidCommandException {
		robotService.moveRobot("lll");
		assertEquals(FaceEnum.EAST.getValue(), robot.getFace());
	}	
	
	@Test
	public void rotate360Left() throws InvalidCommandException {
		robotService.moveRobot("llll");
		assertEquals(FaceEnum.NORTH.getValue(), robot.getFace());
	}	
	
	@Test(expected=InvalidCommandException.class)
	public void invalidCommand() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("DDD");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void invalidCommandAfterWalk() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("LD");
	}

}
