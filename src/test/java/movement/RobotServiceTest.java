package movement;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.contaazul.customException.InvalidCommandException;
import com.contaazul.customException.RobotCollisionException;
import com.contaazul.enums.FaceEnum;
import com.contaazul.model.Robot;
import com.contaazul.service.RobotService;

public class RobotServiceTest {
	
	private Robot robot;
	private RobotService robotService;
	
	@Before
	public void resetPosition() {
		robot = new Robot();
		this.robotService = new RobotService(this.robot);
	}	

	@Test
	public void commandsMovementTest() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmllm");
		
		assertEquals(1, robot.getYNodePosition());
	}
	
	@Test
	public void commandsMovementTest2() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmllmm");
		
		assertEquals(0, robot.getYNodePosition());
	}	
	
	@Test
	public void commandsMovementTest3() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmllmm");
		
		assertEquals(FaceEnum.SOUTH.getValue(), robot.getFace());
	}
	
	@Test
	public void commandsMovementTest4() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("r");
		
		assertEquals(FaceEnum.EAST.getValue(), robot.getFace());
	}		
	
	@Test(expected=RobotCollisionException.class)
	public void commandsMovementTest5() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmllmmm");
	}
	
	@Test(expected=InvalidCommandException.class)
	public void commandsMovementTest6() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmlhlmm");
		
	}
	
	@Test
	public void commandsMovementTest7() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("rmm");
		
		assertEquals(2, robot.getXNodePosition());
	}	
	
	@Test
	public void commandsMovementTest8() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("rmm");
		
		assertEquals(0, robot.getYNodePosition());
	}	
	
	@Test
	public void commandsMovementTest9() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("rmmmllm");
		
		assertEquals(2, robot.getXNodePosition());
	}	
	
	@Test
	public void commandsMovementTest10() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmmlllm");
		
		assertEquals(1, robot.getXNodePosition());
	}
	
	@Test
	public void commandsMovementTest11() throws InvalidCommandException, RobotCollisionException {
		robotService.moveRobot("mmmlllm");
		
		assertEquals(1, robot.getXNodePosition());
	}	

}
