package movement;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.contaazul.customException.RobotCollisionException;
import com.contaazul.model.Robot;

public class MovementTest {

	Robot robot;
	
	@Before
	public void resetRobot() {
		robot = new Robot();
	}
			
	@Test
	public void oneStepForward() throws RobotCollisionException {
		robot.moveForward();
		assertEquals(1, robot.getYNodePosition());
	}
	
	@Test
	public void threeStepsForward() throws RobotCollisionException {
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		assertEquals(3, robot.getYNodePosition());
	}	
	
	@Test
	public void fiveStepsForward() throws RobotCollisionException {
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		assertEquals(5, robot.getYNodePosition());
	}	
	
	@Test(expected=RobotCollisionException.class)
	public void sixStepsForward() throws RobotCollisionException {
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
		robot.moveForward();
	}	

	@Test
	public void oneStepRight() throws RobotCollisionException {
		robot.moveRight();
		assertEquals(1, robot.getXNodePosition());
	}
	
	@Test
	public void threeStepsRight() throws RobotCollisionException {
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		assertEquals(3, robot.getXNodePosition());
	}	
	
	@Test
	public void fiveStepsRight() throws RobotCollisionException {
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		assertEquals(5, robot.getXNodePosition());
	}	
	
	@Test(expected=RobotCollisionException.class)
	public void sixStepsRight() throws RobotCollisionException {
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
		robot.moveRight();
	}	
	
	@Test(expected=RobotCollisionException.class)
	public void oneStepBackward() throws RobotCollisionException {
		robot.moveBackward();
	}
	
	@Test(expected=RobotCollisionException.class)
	public void oneStepLeft() throws RobotCollisionException {
		robot.moveLeft();
	}	
	
}
